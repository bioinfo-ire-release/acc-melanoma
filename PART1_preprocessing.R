################################################ PANEL #################################################

genes = read.csv('acc-lung-gene-list.txt',header = F)$V1
t = read.table('bed.tsv')
t$diff = t$V3 - t$V2
t$start_next = c(tail(t$V2,nrow(t)-1),tail(t$V3,1))
t$overlap = t$V3 -t$start_next
t[t$overlap < 0, "overlap"] <- 0
t[t$overlap > 200, "overlap"] <-0

################################################ EXOME #################################################

#### mutations 

studies <- c("mskcc.txt","ucla.txt")

f <- function(file){
  df = read.csv2(file,header = T, skip = 0,sep='\t')
    return(df)
}

df <- data.frame(matrix(ncol = 46,nrow = 0))

for (file in studies){
  muts <- f(file)
  muts <- muts[,c("Tumor_Sample_Barcode","Hugo_Symbol","Chromosome","Start_Position","End_Position","Variant_Classification","HGVSp","HGVSc","t_ref_count","t_alt_count")]

df <- rbind(df,muts)}

df$vaf = df$t_alt_count/(df$t_alt_count + df$t_ref_count)
df = df[,c("Tumor_Sample_Barcode","Hugo_Symbol","Chromosome","Start_Position","End_Position","Variant_Classification","HGVSp","HGVSc","vaf")]

df = df[df$vaf > 0.05 & df$Variant_Classification %in% c("Missense_Mutation","Nonsense_Mutation","NONSENSE","Nonstop_Mutation"),]

#### clinical

clinical_cbio = read.csv2('combined_study_clinical_data.tsv',sep='\t')
clinical_cbio = clinical_cbio[,c('Patient.ID','Treatment.Response','Disease.Free.status','Disease.Free.Survival..Months.','Overall.Survival.Status','Overall.Survival..Months.')]
clinical_cbio[is.na(clinical_cbio)] = ""
t1 = sapply(clinical_cbio$Treatment.Response, function(x) str_replace(x,'Responder','R'))
t1 = sapply(t1, function(x) str_replace(x,'nonresponse','NR'))
t1 = sapply(t1, function(x) str_replace(x,'Non-responder','NR'))
t1 = sapply(t1, function(x) str_replace(x,'response','R'))
t2 = sapply(clinical_cbio$Disease.Free.status, function(x) str_replace(x,'0:DiseaseFree','PF'))
t2 = sapply(t2, function(x) str_replace(x,'1:Recurred/Progressed','R'))
t3 = sapply(clinical_cbio$Overall.Survival.Status, function(x) str_replace(x,'0:LIVING','L'))
t3 = sapply(t3, function(x) str_replace(x,'1:DECEASED','D'))

names(t1) <- NULL
names(t2) <- NULL
names(t3) <- NULL

clinical_cbio$Response <- as.factor(t1)
clinical_cbio$PF_status <- as.factor(t2)
clinical_cbio$OS_status <- as.factor(t3)

clinical_cbio = clinical_cbio[,c('Patient.ID','Response','OS_status','PF_status','Overall.Survival.Status','Disease.Free.Survival..Months.','Overall.Survival..Months.')]

#### merge

samples = unique(clinical_cbio$Patient.ID)
df = df[df$Tumor_Sample_Barcode %in% samples,]
df_cbio = data.frame(matrix(nrow = length(samples),ncol=1))
rownames(df_cbio) = samples

for (gene in genes){
  vec = c()
  for (sample in samples){
    if (nrow(df[df$Hugo_Symbol == gene & df$Tumor_Sample_Barcode == sample,]) >= 1 ){
      filt = df[df$Hugo_Symbol == gene & df$Tumor_Sample_Barcode == sample,]
      vec = c(vec,max(filt$vaf))
    }
    else {
      vec = c(vec,0)
    }
  }
  df_cbio = cbind(df_cbio,vec)
}

colnames(df_cbio) = c('id',genes)
df_cbio$id <- NULL
write.table(df_cbio,'binary_mutations_cbioportal.txt')

dataset_cbio = merge(df_cbio, clinical_cbio,by.x = 0, by.y = 'Patient.ID')

dataset_cbio$Treatment.Response <- NULL
write.csv(dataset_cbio,"wes_dataset_encoded.csv")

### tmb calculation

df_05del = df[df$vaf > 0.05,]
df_15del = df[df$vaf > 0.15,]


tmb05del = (table(df_05del$Tumor_Sample_Barcode)*1000000)/50000000
tmb15del = (table(df_15del$Tumor_Sample_Barcode)*1000000)/50000000

write.csv(df,'all_variants_exome_new.csv')

### tmb on lung genes

df_lung = df[df$Hugo_Symbol %in% genes,]
df_lung_05del = df_lung[df_lung$vaf > 0.05,]
df_lung_15del = df_lung[df_lung$vaf > 0.15,]

tmb05del_lung = (table(df_lung_05del$Tumor_Sample_Barcode)*1000000)/(sum(t$diff)-sum(t$overlap))
tmb15del_lung = (table(df_lung_15del$Tumor_Sample_Barcode)*1000000)/(sum(t$diff)-sum(t$overlap))

tmb = data.frame(table(df$Tumor_Sample_Barcode))
tmb = merge(tmb, data.frame(tmb05del),by.x = 'Var1',by.y='Var1')
tmb = merge(tmb, data.frame(tmb15del),by.x = 'ID',by.y='Var1')
tmb = merge(tmb, data.frame(tmb05del_lung),by.x = 'ID',by.y='Var1')
tmb = merge(tmb, data.frame(tmb15del_lung),by.x = 'ID',by.y='Var1')
colnames(tmb) =  c('ID','nmut','tmb_exome_05_del','tmb_exome_15_del','tmb_lung_05_del','tmb_lung_15_del')
write.csv(tmb,'tmb_validation.csv')

final_cbio = merge(tmb,dataset_cbio,by.x = 'ID', by.y= 'Row.names' )
final_cbio$Overall.Survival.Status <- NULL
colnames(final_cbio) <- c(head(colnames(final_cbio),-2),'PF_months','OS_months')
write.csv(final_cbio,'wes_final_vaf.csv')

### Mode

ds = df
bi = c()
tri = c()
uni = c()
multi = c()
amod = c()

for (sample in unique(ds$Tumor_Sample_Barcode)){
    bi = c(bi,is.bimodal(ds[ds$Tumor_Sample_Barcode == sample,'vaf'],min.size=0.1))
    tri = c(tri,is.trimodal(ds[ds$Tumor_Sample_Barcode == sample,'vaf'],min.size=0.1))
    uni = c(uni,is.unimodal(ds[ds$Tumor_Sample_Barcode == sample,'vaf'],min.size=0.1))
    multi = c(multi,is.multimodal(ds[ds$Tumor_Sample_Barcode == sample,'T.AF'],min.size=0.1))
    amod = c(amod,is.amodal(ds[ds$Tumor_Sample_Barcode == sample,'vaf'],min.size=0.1))
  if (length(ds[ds$Tumor_Sample_Barcode == sample,'vaf']) > 1){
    plot(density(as.numeric(ds[ds$Tumor_Sample_Barcode == sample,'vaf'])))
  }
}

multi = multi - bi - tri
multi_index = which(multi==1)
bi_index = which(bi==T)
tri_index = which(tri==T)
uni_index = which(uni==T)
amod_index = which(amod==T)

modes_sample = rep('',length(unique(ds$Tumor_Sample_Barcode)))
names(modes_sample) = unique(ds$Tumor_Sample_Barcode)
modes_sample[multi_index] = 'multi'
modes_sample[bi_index] = 'bi'
modes_sample[tri_index] = 'tri'
modes_sample[uni_index] = 'uni'
modes_sample[amod_index] = 'amod'

final_cbio = merge(final_cbio,data.frame(modes_sample), by.x = 'ID',by.y = 0)

################################################ PANEL #################################################

#### mutations

df = read.csv('msk-pannello.txt',header = T, skip = 0,sep='\t')
df$vaf = df$t_alt_count/(df$t_alt_count + df$t_ref_count)
df = df[,c("Tumor_Sample_Barcode","Hugo_Symbol","Chromosome","Start_Position","End_Position","Variant_Classification","HGVSp","HGVSc","vaf")]
df = df[df$vaf > 0.05 & df$Variant_Classification %in% c("Missense_Mutation","Nonsense_Mutation","NONSENSE","Nonstop_Mutation"),]
df$Tumor_Sample_Barcode <- unlist(lapply(df$Tumor_Sample_Barcode,function(x) str_replace(x,'-T01-IM6','')))

#### clinical

clinical_cbio = read.csv2('data_clinical_pannello.txt',sep='\t')
clinical_cbio = clinical_cbio[,c('PATIENT_ID','OS_STATUS','OS_MONTHS')]
t3 = sapply(clinical_cbio$OS_STATUS, function(x) str_replace(x,'0:LIVING','L'))
t3 = sapply(t3, function(x) str_replace(x,'1:DECEASED','D'))
names(t3) <- NULL
clinical_cbio$OS_STATUS <- as.factor(t3)

#### merge
samples = unique(clinical_cbio$PATIENT_ID)
df = df[df$Tumor_Sample_Barcode %in% samples,]
df_cbio = data.frame(matrix(nrow = length(samples),ncol=1))
rownames(df_cbio) = samples

for (gene in genes){
  vec = c()
  for (sample in samples){
    if (nrow(df[df$Hugo_Symbol == gene & df$Tumor_Sample_Barcode == sample,]) >= 1 ){
      filt = df[df$Hugo_Symbol == gene & df$Tumor_Sample_Barcode == sample,]
      vec = c(vec,max(filt$vaf))
    }
    else {
      vec = c(vec,0)
    }
  }
  df_cbio = cbind(df_cbio,vec)
}

colnames(df_cbio) = c('id',genes)
df_cbio$id <- NULL
df_cbio = df_cbio[,genes]
dataset_cbio = merge(df_cbio, clinical_cbio,by.x = 0, by.y = 'PATIENT_ID')

#### tmb

df_05del = df[df$vaf > 0.05,]
df_15del = df[df$vaf > 0.15,]


tmb05del = (table(df_05del$Tumor_Sample_Barcode)*1000000)/50000000
tmb15del = (table(df_15del$Tumor_Sample_Barcode)*1000000)/50000000

#### tmb on lung genes

df_lung_05del = df[df$vaf > 0.05,]
df_lung_15del = df[df$vaf > 0.15,]

tmb05del_lung = (table(df_lung_05del$Tumor_Sample_Barcode)*1000000)/(sum(t$diff)-sum(t$overlap))
tmb15del_lung = (table(df_lung_15del$Tumor_Sample_Barcode)*1000000)/(sum(t$diff)-sum(t$overlap))

tmb = data.frame(table(df$Tumor_Sample_Barcode))
tmb = merge(tmb, data.frame(tmb05del_lung),by.x = 'Var1',by.y='Var1')
colnames(tmb) = c('ID','nmut','tmb_lung_05_del')
tmb = merge(tmb, data.frame(tmb15del_lung),by.x = 'ID',by.y='Var1')
colnames(tmb) = c('ID','nmut','tmb_lung_05_del','tmb_lung_15_del')

write.csv(tmb,'tmb_msk_panel.csv')

final_cbio = merge(tmb,dataset_cbio,by.x = 'ID', by.y= 'Row.names' )
write.csv(final_cbio,'cbioportal_panel_final_vaf.csv')

################################################ ACC PANEL #################################################

#### clinical
clinical = read.csv2('acc-mela-followup.csv')
ds = read.csv2('ALL.IR-all_called.noCNV.breakmulti.clean.ir_refilter.cosm_id.pvalue.annovar.hg19_multianno.noOtherinfo.sorted.oncokb.VAF.Center.somatic_confidence.maf.csv',sep=';')
coverage = ds %>%
  group_by( Tumor_Sample_Barcode ) %>%
  summarise( Median=median(T.DP) )
sd(coverage$Median)

samples = unique(clinical[,'barcode'])

#### mutations

ds = ds[ds$Tumor_Sample_Barcode %in% samples,]
ds = ds[ds$T.AF > 0.05,]
ds = ds[! ds$Hugo_Symbol %in% c("CRIPAK","PRKCZ","PRKCZ-AS1"),]

ds = ds[ds$confident_somatic_according_to_IR_only == 'somatic',]
table(ds$confident_somatic_according_to_IR_only)
ds = ds[ds$Variant_Classification %in% c('Missense_Mutation','Nonsense_Mutation','In_Frame_Ins','Frame_Shift_Ins'),]
genes = unique(ds$Hugo_Symbol)

tmb05del = (table(ds$Tumor_Sample_Barcode)*1000000)/target
tmb15del = (table(ds[ds$T.AF>0.15,'Tumor_Sample_Barcode'])*1000000)/target


#### binary matrix
df = data.frame(matrix(nrow = length(samples),ncol=1))
rownames(df) = samples

for (gene in genes){
  vec = c()
  for (sample in samples){
    if (nrow(ds[ds$Hugo_Symbol == gene & ds$Tumor_Sample_Barcode == sample,]) >= 1 ){
      filt = ds[ds$Hugo_Symbol == gene & ds$Tumor_Sample_Barcode == sample,]
      vec = c(vec,1)
    }
    else {
      vec = c(vec,0)
    }
  }
  df = cbind(df,vec)
}

colnames(df) = c('id',genes)
df$id <- NULL
write.table(df,'mutations_acc.txt')

#### vaf  matrix

df = data.frame(matrix(nrow = length(samples),ncol=1))
rownames(df) = samples

for (gene in genes){
  vec = c()
  for (sample in samples){
    if (nrow(ds[ds$Hugo_Symbol == gene & ds$Tumor_Sample_Barcode == sample,]) >= 1 ){
      filt = ds[ds$Hugo_Symbol == gene & ds$Tumor_Sample_Barcode == sample,]
      vec = c(vec,max(as.numeric(filt$T.AF)))
    }
    else {
      vec = c(vec,0)
    }
  }
  df = cbind(df,vec)
}

colnames(df) = c('id',genes)
df$id <- NULL
write.table(df,'vaf_mutations_acc.txt')

#### clinical

df_filt = df

clinical_filt = clinical[,c('barcode','therapy_1','DFS','PFS_days','OS','OS_days')]

tmb = merge(as.data.frame(tmb05del),as.data.frame(tmb15del), by.x = 'Var1', by.y = 'Var1')
colnames(tmb) <- c('barcode','tmb_05_damaging','tmb_15_damaging')
df_filt = merge(df_filt,tmb, by.x = 0, by.y = 'barcode')

final = merge(df_filt,clinical_filt,by.x = 'Row.names', by.y = 'barcode')
final$treat_resp = final$therapy_1
t = sapply(final$therapy_1, function(x) str_replace(x,'SD','NR'))
t = sapply(t, function(x) str_replace(x,'PR','R'))
t = sapply(t, function(x) str_replace(x,'CR','R'))
t = sapply(t, function(x) str_replace(x,'PD','NR'))
names(t) <- NULL
final$Response <- as.factor(t)

t = sapply(final$therapy_1, function(x) str_replace(x,'SD','R'))
t = sapply(t, function(x) str_replace(x,'PR','R'))
t = sapply(t, function(x) str_replace(x,'CR','R'))
t = sapply(t, function(x) str_replace(x,'PD','NR'))
names(t) <- NULL

final$PF_status <- as.factor(final$DFS)
final$DFS <- NULL

t = sapply(final$OS, function(x) gsub(1,'D',x))
t = sapply(t, function(x) gsub(0,'L',x))
names(t) <- NULL

final$OS_status <- as.factor(t)
final$OS <- NULL

write.csv(final,"ACC_final_vaf_target-immuno.csv")

#### mode

bi = c()
tri = c()
uni = c()
multi = c()
amod = c()

for (sample in unique(ds$Tumor_Sample_Barcode)){
      bi = c(bi,is.bimodal(ds[ds$Tumor_Sample_Barcode == sample,'T.AF'],min.size=0.1))
    tri = c(tri,is.trimodal(ds[ds$Tumor_Sample_Barcode == sample,'T.AF'],min.size=0.1))
    uni = c(uni,is.unimodal(ds[ds$Tumor_Sample_Barcode == sample,'T.AF'],min.size=0.1))
    multi = c(multi,is.multimodal(ds[ds$Tumor_Sample_Barcode == sample,'T.AF'],min.size=0.1))
    amod = c(amod,is.amodal(ds[ds$Tumor_Sample_Barcode == sample,'T.AF'],min.size=0.1))
  if (length(ds[ds$Tumor_Sample_Barcode == sample,'T.AF']) > 1){
    plot(density(as.numeric(ds[ds$Tumor_Sample_Barcode == sample,'T.AF'])))
  }
}

multi = multi - bi - tri
multi_index = which(multi==1)
bi_index = which(bi==T)
tri_index = which(tri==T)
uni_index = which(uni==T)
amod_index = which(amod==T)

modes_sample = rep('',length(unique(ds$Tumor_Sample_Barcode)))
names(modes_sample) = unique(ds$Tumor_Sample_Barcode)
modes_sample[multi_index] = 'multi'
modes_sample[bi_index] = 'bi'
modes_sample[tri_index] = 'tri'
modes_sample[uni_index] = 'uni'
modes_sample[amod_index] = 'amod'

final = merge(final,data.frame(modes_sample), by.x = 'Row.names',by.y = 0)
