# ACC-melanoma

# Part 1
- Description of the cohort (treatment response, mutations...)
- Comparison with other WES and target melanoma cohorts (incidence of mutations)
- Pharmacological actionability of the cohort

# Part 2
- TMB for ICI response
- Lasso-COX model for feature selection
- RF for survival prediction
- VIMP for feature importance validation
